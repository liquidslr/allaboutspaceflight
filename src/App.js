import React from "react";
import axios from "axios";
import LinearProgress from "@material-ui/core/LinearProgress";

import Question from "./question";
import Header from "./header";
import Footer from "./footer";

class App extends React.Component {
  state = { data: [] };

  componentDidMount() {
    axios
      .get("http://api.padhai.io/api/v1/intern/internship-test")
      .then(res => this.setState({ data: [res.data] }));
  }

  render() {
    const { data } = this.state;

    return (
      <div className="App">
        <Header />
        {!data.length ? (
          <LinearProgress />
        ) : (
          data.map(ques => {
            return <Question key={ques.creation_date} question={ques} />;
          })
        )}
        <Footer />
      </div>
    );
  }
}

export default App;
