import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";

import { withStyles } from "@material-ui/core/styles";

import Option from "./options";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    height: "max-content",
    marginBottom: "5rem",
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  image: {
    height: "20rem",
    width: "auto"
  },
  imageC: {
    marginBottom: 50
  },
  wrapper: {
    height: "max-content"
  }
});

function PaperSheet(props) {
  const { classes, question } = props;

  return (
    <div className={classes.wrapper}>
      <Paper className={classes.root} elevation={1}>
        <Typography variant="h5" component="h3">
          {question.prompt_md}
        </Typography>
        <div className={classes.imageC}>
          <img src={question.image1} alt="image1" className={classes.image} />
          <img src={question.image2} alt="image2" className={classes.image} />
        </div>
        <Grid className={classes.option} container spacing={24}>
          {question.options.map(ans => {
            return <Option key={ans.id} ans={ans} />;
          })}
        </Grid>
      </Paper>
    </div>
  );
}

PaperSheet.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PaperSheet);
