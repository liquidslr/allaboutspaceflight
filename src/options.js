import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Snackbar from "@material-ui/core/Snackbar";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    color: theme.palette.text.secondary
  }
});

class Checkboxes extends React.Component {
  state = {
    checked: false,
    open: false,
    message: "",
    vertical: "top",
    horizontal: "center"
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });

    setTimeout(() => this.setState({ open: false }), 1000);
  };

  render() {
    const { ans, classes } = this.props;
    const { vertical, horizontal } = this.state;

    let open = false;
    if (this.state.checked && !this.props.ans.is_correct) {
      open = true;
    }

    setTimeout(() => this.setState({ checked: false }), 2500);

    return (
      <>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          onClose={this.handleClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">Incorrect Answer</span>}
        />
        <Grid item xs={12} sm={5}>
          <Paper className={classes.paper}>
            <Checkbox
              checked={this.state.checked}
              onChange={this.handleChange("checked")}
              value="checked"
            />
            {ans.value}
          </Paper>
        </Grid>
      </>
    );
  }
}

export default withStyles(styles)(Checkboxes);
